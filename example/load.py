import joyo

"""
常用漢字一覧を取得する.
常用漢字一覧を取得されていなければ, 取得する.
"""

kanjis = joyo.load()

assert '一' in kanjis
