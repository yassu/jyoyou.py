from pathlib import Path

import joyo

"""
パスを指定して 常用漢字一覧を取得する.
常用漢字一覧を取得されていなければ, 取得する.
"""

kanjis = joyo.load(Path('/tmp/klmabchkl'))

assert '一' in kanjis
