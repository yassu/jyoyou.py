from pathlib import Path

import joyo

"""
保存用のパスを指定して 常用漢字一覧を保存する
"""

joyo.update(Path('/tmp/klmabcdef'))   # パスを指定することもできる
